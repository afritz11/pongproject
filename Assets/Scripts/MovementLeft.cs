﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementLeft : MonoBehaviour {
    public GameObject leftPlayer;
    public float moveSpeed;
    public bool isPasued = false;
    void Update ()
    {
        if (isPasued == false)
        {
            if (Input.GetKey(KeyCode.W) && gameObject.transform.position.z <= 4.4)
            {
                leftPlayer.transform.Translate(transform.forward * moveSpeed);
            }
            if (Input.GetKey(KeyCode.S) && gameObject.transform.position.z >= -4.4 )
            {
                leftPlayer.transform.Translate(-transform.forward * moveSpeed);
            }
        } 
     }
}
