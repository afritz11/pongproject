﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp_Paddle : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;
	// Use this for initialization
	void Start ()
    {
        player1 = GameObject.FindWithTag("Left");
        player2 = GameObject.FindWithTag("Right");
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    
    public void OnTriggerEnter(Collider other)
    {
        if(other.transform.CompareTag("ball"))
        {
            player1.transform.localScale = new Vector3(-.23f, 0.5f, 1.2f);
            player2.transform.localScale = new Vector3(-.23f, 0.5f, 1.2f);
            Destroy(gameObject);
        }
    }
}
