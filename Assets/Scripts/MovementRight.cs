﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementRight : MonoBehaviour
{
    public GameObject rightPlayer;
    public float moveSpeed;
    public bool isPaused = false;
	void Update ()
    {
        if (isPaused == false)
        {
            if (Input.GetKey(KeyCode.UpArrow) && gameObject.transform.position.z <= 4.4)
            {
                rightPlayer.transform.Translate(transform.forward * moveSpeed);
            }
            if (Input.GetKey(KeyCode.DownArrow) && gameObject.transform.position.z >= -4.4)
            {
                rightPlayer.transform.Translate(-transform.forward * moveSpeed);
            }
        }
     }
}
