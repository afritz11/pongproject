﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public float player1Lives = 3;
    public float player2Lives = 3;
    public int nextScene;
    public Text player1Text;
    public Text player2Text;
    
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    public void OnTriggerEnter(Collider other)
    {
        if(other.transform.CompareTag("RightWall"))
        {
            player1Lives -= 1;
        }

        if (other.transform.CompareTag("LeftWall"))
        {
            player2Lives -= 1;
        }

        if(player1Lives <= 0 || player2Lives <= 0 )
        {
            SceneManager.LoadScene(nextScene);
        }
        updateScore();

    }

    public void updateScore()
    {
        player1Text.text = "Player1 Lives: " + player1Lives.ToString();
        player2Text.text = "Player2 Lives: " + player2Lives.ToString();
    }
}
